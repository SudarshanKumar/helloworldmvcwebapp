﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelloWorldMVCWebApp.Migrations
{
    public partial class classComputers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Books_DB",
                table: "Books_DB");

            migrationBuilder.RenameTable(
                name: "Books_DB",
                newName: "Book");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Book",
                table: "Book",
                column: "BookId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Book",
                table: "Book");

            migrationBuilder.RenameTable(
                name: "Book",
                newName: "Books_DB");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Books_DB",
                table: "Books_DB",
                column: "BookId");
        }
    }
}

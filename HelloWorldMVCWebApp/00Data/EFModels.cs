﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloWorldMVCWebApp._00Data
{
    public class Book
    {
        public int BookId { get; set; }
        [StringLength(50),]
        public string Title { get; set; }

        public string BookPublisherTitle { get; set; }
        public string Author { get; set; }
    }

    public class Computer
    {
        public int ComputerId { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
       public string BookPublisherTitle { get; set; }
        public string Author { get; set; }
    }
}
